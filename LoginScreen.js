import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import PushNotification from 'react-native-push-notification';

const LoginScreen = () => {
  const [valueFour, setValueOne] = useState('');
  const [valueFive, setValueTwo] = useState('');

  const [data, setData] = useState([]);

  const navigation = useNavigation();

  useEffect(() => {
    callUsersApi();
    createChannels();
  }, []);

  const createChannels = () => {
    PushNotification.createChannel({
      channelId: 'test',
      channelName: 'test channel',
    });
  };

  function callUsersApi() {
    fetch('http://192.168.0.188:5000/users', {
      method: 'GET',
    })
      .then(response => response.json())
      .then(responseJson => {
        setData(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  }

  function checkLogin() {
    var verfiedUser = data.find(x => x.email == valueFour);

    if (valueFour == '' || valueFive == '') {
      Alert.alert('Please Enter all the fields');
    } else if (verfiedUser != undefined && verfiedUser.email == valueFour) {
      var verifiedUserName = verfiedUser.email;
      var verifiedUserJob = verfiedUser.jobTitle;
      setValueOne('');
      setValueTwo('');
      navigation.navigate('Home', {verifiedUserName, verifiedUserJob});
    } else {
      Alert.alert('Invalid login');
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 80,
        }}>
        <Text
          style={{
            padding: 30,
            fontWeight: '700',
            fontSize: 20,
            color: 'black',
          }}>
          LOGIN
        </Text>
        <View
          style={{
            backgroundColor: 'white',
            width: '90%',
            alignItems: 'center',
            height: 300,
            justifyContent: 'center',
            borderRadius: 10,
          }}>
          <TextInput
            placeholder={'Email'}
            value={valueFour}
            style={styles.textInput}
            onChangeText={text => setValueOne(text)}
          />
          <TextInput
            placeholder={'Password'}
            value={valueFive}
            style={styles.textInput}
            secureTextEntry={true}
            onChangeText={text => setValueTwo(text)}
          />
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              style={styles.addBtn}
              onPress={() => checkLogin()}>
              <Text style={styles.addText}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.addBtn}
              onPress={() => navigation.navigate('Register')}>
              <Text style={styles.addText}>Register</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d9f1ff',
  },
  addBtn: {
    margin: 10,
    width: '35%',
    height: 45,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
    borderRadius: 5,
  },
  addText: {
    alignSelf: 'center',
    color: 'white',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderBottomWidth: 1,
    padding: 10,
    width: '75%',
    height: 40,
    borderBottomColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    justifyContent: 'center',
  },
  textStyle: {
    margin: 10,
  },
});

export default LoginScreen;

import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
  Modal,
} from 'react-native';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';

const ProjectDetails = ({route}) => {
  const [response, setResponse] = useState([]);
  var screenView = useIsFocused();

  const [users, setUsers] = useState([]);
  const [focus, setFocus] = useState(screenView);
  const [projectStatus, setProjectStatus] = useState([]);
  const [todoTasks, setTodoTasks] = useState('');

  const {verifiedUserName} = route.params;

  const navigation = useNavigation();

  useEffect(() => {
    callTaskGet();
    setFocus(screenView);
  }, [focus, screenView]);

  function callTaskGet() {
    fetch('http://192.168.0.188:5000/task', {
      method: 'GET',
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson != null) {
          setResponse(responseJson);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  function callTask() {
    console.log('function called', todoTasks);
    fetch('http://192.168.0.188:5000/task/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Connection: 'keep-alive',
      },
      body: JSON.stringify({
        taskname: todoTasks,
        status: 'Pending',
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson == 'Task added!') {
          callTaskGet();
          setTodoTasks('');
          console.log('res', responseJson);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  function callDelete(item) {
    var id = item._id;
    fetch('http://192.168.0.188:5000/task/' + id, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Connection: 'keep-alive',
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson == 'Project deleted!') {
          callTaskGet();
          console.log('deleted');
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  function callUpdate(item) {
    var id = item._id;
    fetch('http://192.168.0.188:5000/task/update/' + id, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Connection: 'keep-alive',
      },
      body: JSON.stringify({
        taskname: item.taskname,
        status: item.status == 'Pending' ? 'Completed' : 'Pending',
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson == 'Task updated!') {
          callTaskGet();
          console.log('updated');
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={{backgroundColor: 'white', margin: 10, borderRadius: 5}}>
        <TextInput
          value={todoTasks}
          style={styles.textInput}
          onChangeText={text => setTodoTasks(text)}
          placeholder={'Enter tasks'}
        />
        <TouchableOpacity
          style={styles.textInputAddBtn}
          onPress={() => callTask()}>
          <Text style={styles.addText}>Add</Text>
        </TouchableOpacity>
      </View>
      {response != null && response.length > 0 ? (
        <FlatList
          data={response}
          keyExtractor={items => items.id}
          renderItem={({item}) => (
            <TouchableOpacity onPress={() => callUpdate(item)}>
              <View style={styles.touchableOpacityStyle}>
                <Text
                  style={
                    item.status == 'Completed'
                      ? styles.textStyle2
                      : styles.textStyle
                  }>
                  {item.taskname}
                </Text>
                {item.status == 'Completed' ? (
                  <Icon name="done" color={'black'} size={20} />
                ) : null}
                <Icon
                  name="delete"
                  color={'black'}
                  size={20}
                  onPress={() => callDelete(item)}
                />
              </View>
            </TouchableOpacity>
          )}
        />
      ) : null}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d9f1ff',
  },
  addBtn: {
    margin: 10,
    width: '95%',
    height: 45,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
    borderRadius: 5,
  },
  addText: {
    alignSelf: 'center',
    color: 'white',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderBottomWidth: 1,
    padding: 10,
    width: '90%',
    height: 40,
    borderBottomColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 40,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
    borderRadius: 5,
  },
  touchableOpacityStyle: {
    height: 40,
    backgroundColor: 'white',
    padding: 5,
    margin: 5,
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textStyle2: {
    color: 'black',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  textStyle: {
    color: 'black',
  },
  textStyle1: {
    margin: 10,
    color: 'blue',
    fontWeight: '400',
  },
});

export default ProjectDetails;

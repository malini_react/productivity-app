import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
} from 'react-native';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import dateFormat from 'dateformat';
import PushNotification from 'react-native-push-notification';
import {Calendar} from 'react-native-calendars';

const Planner = ({route}) => {
  const {verifiedUserName} = route.params;

  var screenView = useIsFocused();

  const [click, setClick] = useState(false);
  const [valueOne, setValueOne] = useState('');
  const [valueTwo, setValueTwo] = useState('');
  const [dummy, setDummy] = useState('dummy');
  const [data, setData] = useState([]);
  const [response, setResponse] = useState([]);
  const [monthView, setMonthView] = useState(false);
  const [markedDate, setMarkedDate] = useState(false);

  const [focus, setFocus] = useState(screenView);

  const navigation = useNavigation();

  useEffect(() => {
    callProjectGet();
    setFocus(screenView);
    callNotification();
    callMarked();
  }, [screenView, focus]);

  function callMarked() {
    let markedDates = {};
    response.map(x => {
      var jobDate = x.planschedule;
      jobDate = dateFormat(jobDate, 'yyyy-mm-dd');
      markedDates[jobDate] = {
        marked: true,
        dotColor: '#0da2ff',
        selected: false,
      };
    });
    setMarkedDate(markedDates);
    console.log('mark', markedDates);
  }

  const callNotification = () => {
    var date = new Date();
    response.map(x => {
      if (
        dateFormat(x.planschedule, 'dd-mm-yyyy') ==
          dateFormat(date, 'dd-mm-yyyy') &&
        dateFormat(x.planschedule, 'hh:MM') >= dateFormat(date, 'hh:MM')
      ) {
        // var date2 = new Date(x.planschedule);
        console.log('date2');
        PushNotification.localNotificationSchedule({
          channelId: 'test',
          title: 'My planner',
          message: x.planname,
          date: new Date(x.planschedule),
        });
      }
    });
  };

  function callProjectGet() {
    fetch('http://192.168.0.188:5000/planner', {
      method: 'GET',
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson != null) {
          setResponse(responseJson);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  function callDelete(item) {
    var id = item._id;
    fetch('http://192.168.0.188:5000/project/' + id, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Connection: 'keep-alive',
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson == 'Project deleted!') {
          callProjectGet();
          console.log('deleted');
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  return (
    <SafeAreaView style={styles.container}>
      {monthView ? (
        <Calendar
          theme={{
            backgroundColor: '#fff',
            calendarBackground: '#fff',
            selectedDayBackgroundColor: '#0da2ff',
            selectedDayTextColor: 'white',
            todayBackgroundColor: '#0da2ff',
            todayTextColor: 'black',
            arrowColor: '#0da2ff',
            dotStyle: {marginTop: 8},
          }}
          //   markingType="period"
          markedDates={markedDate}
          current={Date()}
          minDate={'2020-05-10'}
          maxDate={'2030-05-30'}
          onDayPress={day => {
            // getSelectedDayEvents(day.dateString);
          }}
          onDayLongPress={day => {
            // getRangeEvents(day);
          }}
          hideExtraDays={true}
          firstDay={1}
          onPressArrowLeft={subtractMonth => subtractMonth()}
          onPressArrowRight={addMonth => addMonth()}
          disableAllTouchEventsForDisabledDays={true}
          renderHeader={date => {
            var calenderMonth = dateFormat(date, 'mmm yyyy');
            return (
              <View>
                <Text>{calenderMonth}</Text>
              </View>
            );
          }}
        />
      ) : null}
      <View>
        <TouchableOpacity
          style={styles.addBtn}
          onPress={() => setMonthView(!monthView)}>
          <Text style={[styles.addText, {fontWeight: '400', fontSize: 14}]}>
            Show calender
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.addBtn}
          onPress={() =>
            navigation.navigate('NewPlan', {
              verifiedUserName,
            })
          }>
          <Text style={[styles.addText, {fontWeight: '400', fontSize: 14}]}>
            Add new plan
          </Text>
        </TouchableOpacity>
      </View>
      <View>
        {response != null && response.length > 0 ? (
          <FlatList
            data={response}
            keyExtractor={item => item.id}
            renderItem={({item}) => (
              <View
                style={{
                  padding: 10,
                  backgroundColor: 'white',
                  margin: 10,
                  borderRadius: 5,
                }}>
                <TouchableOpacity
                  style={styles.touchableOpacityStyle}
                  onPress={() => {
                    navigation.navigate('NotesDetail', {item});
                  }}>
                  {/* <View style={{marginTop: 10, marginBottom: 10}}> */}
                  {/* <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={styles.textStyle}>{item.planame}</Text>
                      <TouchableOpacity onPress={() => callDelete(item)}>
                        <Icon name="delete" color={'black'} size={20} />
                      </TouchableOpacity>
                    </View> */}
                  <Text numberOfLines={2} style={styles.textStyle}>
                    {item.planname}
                  </Text>
                  <Text style={(styles.textStyle, {alignSelf: 'flex-end'})}>
                    {dateFormat(item.planschedule, 'mmm dd, yyyy, hh:MM')}
                  </Text>
                  {/* </View> */}
                </TouchableOpacity>
              </View>
            )}
          />
        ) : null}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d9f1ff',
  },
  addBtn: {
    margin: 10,
    width: '95%',
    height: 40,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
    borderRadius: 5,
    // alignItems: 'flex-end',
    // alignSelf: 'flex-end',
  },
  addText: {
    alignSelf: 'center',
    color: 'white',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '60%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 40,
    justifyContent: 'center',
  },
  textStyle: {
    margin: 2,
    color: 'black',
  },
  textStyle1: {
    margin: 10,
    color: 'blue',
    fontWeight: '400',
  },
});

export default Planner;

const router = require('express').Router();
let Planner = require('../models/planner.model');

router.route('/').get((req, res) => {
  Planner.find()
    .then(planner => res.json(planner))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
  Planner.findById(req.params.id)
    .then(planner => res.json(planner))
    .catch(err => res.status(400).json('Error: ' + err));
});
router.route('/:id').delete((req, res) => {
  Planner.findByIdAndDelete(req.params.id)
    .then(() => res.json('Planner deleted!'))
    .catch(err => res.status(400).json('Error: ' + err));
});
router.route('/update/:id').post((req, res) => {
  Planner.findById(req.params.id)
    .then(planner => {
      planner.planname = req.body.planname;
      planner.planschedule = req.body.planschedule;
      planner.username = req.body.username;
      planner.status = req.body.status;
      planner
        .save()
        .then(() => res.json('Planner updated!'))
        .catch(err => res.status(400).json('Error: ' + err));
    })
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
  const planname = req.body.planname;
  const planschedule = req.body.planschedule;
  const username = req.body.username;
  const status = req.body.status;

  const newProject = new Planner({planname, planschedule, username, status});

  newProject
    .save()
    .then(() => res.json('Planner added!'))
    .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const plannerSchema = new Schema(
  {
    planname: {
      type: String,
      required: true,
    },
    planschedule: {
      type: Date,
      required: true,
    },
    username: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  },
);

const Planner = mongoose.model('Planner', plannerSchema);

module.exports = Planner;

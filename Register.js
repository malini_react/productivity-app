import {NavigationContainer} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const Register = () => {
  const [valueOne, setValueOne] = useState('');
  const [valueTwo, setValueTwo] = useState('');
  const [valueThree, setValueThree] = useState('');
  const [valueFour, setValueFour] = useState('');
  const [valueFive, setValueFive] = useState('');
  const [valueSix, setValueSix] = useState('');

  const [data, setData] = useState([]);

  const navigation = useNavigation();

  function registerUser() {
    fetch('http://192.168.0.188:5000/users/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Connection: 'keep-alive',
      },
      body: JSON.stringify({
        userId: valueFive,
        firstname: valueThree,
        lastname: valueFour,
        email: valueOne,
        jobTitle: valueSix,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson == 'User added!') {
          navigation.navigate('Home', {
            verifiedUserName: 'malini@gmail.com',
            verifiedUserJob: 'admin',
          });
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 50,
        }}>
        <TextInput
          placeholder={'Id'}
          value={valueFive}
          style={styles.textInput}
          onChangeText={text => setValueFive(text)}
        />
        <TextInput
          placeholder={'Firstname'}
          value={valueThree}
          style={styles.textInput}
          onChangeText={text => setValueThree(text)}
        />
        <TextInput
          placeholder={'Lastname'}
          value={valueFour}
          style={styles.textInput}
          onChangeText={text => setValueFour(text)}
        />
        <TextInput
          placeholder={'Email'}
          value={valueOne}
          style={styles.textInput}
          onChangeText={text => setValueOne(text)}
        />
        <TextInput
          placeholder={'Designation'}
          value={valueSix}
          style={styles.textInput}
          onChangeText={text => setValueSix(text)}
        />
        <TouchableOpacity style={styles.addBtn} onPress={() => registerUser()}>
          <Text style={styles.addText}>Add</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  addBtn: {
    margin: 10,
    width: '75%',
    height: 45,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
  },
  addText: {
    alignSelf: 'center',
    color: 'white',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '75%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    justifyContent: 'center',
  },
  textStyle: {
    margin: 10,
  },
});

export default Register;

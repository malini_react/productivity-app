import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
  Button,
  Platform,
} from 'react-native';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import dateFormat from 'dateformat';
import DateTimePicker from '@react-native-community/datetimepicker';

const NewPlan = ({route}) => {
  const {verifiedUserName} = route.params;

  var screenView = useIsFocused();

  const [click, setClick] = useState(false);
  const [valueOne, setValueOne] = useState('');
  const [valueTwo, setValueTwo] = useState('');
  const [dummy, setDummy] = useState('dummy');
  const [data, setData] = useState([]);
  const [response, setResponse] = useState([]);

  const [focus, setFocus] = useState(screenView);
  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  const navigation = useNavigation();

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
    console.log('vlaue', currentDate);
  };

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };

  useEffect(() => {
    // callProjectGet();
    // setFocus(screenView);
  }, [screenView, focus]);

  function callProjectGet() {
    fetch('http://192.168.0.188:5000/project', {
      method: 'GET',
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson != null) {
          setResponse(responseJson);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  function callDelete(item) {
    var id = item._id;
    fetch('http://192.168.0.188:5000/project/' + id, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Connection: 'keep-alive',
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson == 'Project deleted!') {
          callProjectGet();
          console.log('deleted');
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  function callSave() {
    if (date != '' && valueTwo != '') {
      callProject();
    } else {
      Alert.alert('Please enter all the fields');
    }
  }

  function callProject() {
    fetch('http://192.168.0.188:5000/planner/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Connection: 'keep-alive',
      },
      body: JSON.stringify({
        planname: valueTwo,
        planschedule: date,
        username: verifiedUserName,
        status: 'Pending',
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson == 'Planner added!') {
          console.log('Add project success');
        } else {
          console.log('Add project failed');
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          backgroundColor: 'white',
          height: 580,
          margin: 10,
          borderRadius: 5,
        }}>
        <TextInput
          value={valueTwo}
          style={styles.textInput1}
          onChangeText={text => setValueTwo(text)}
          placeholder={'Title'}
        />
        <TextInput
          value={dateFormat(date, 'mmm dd, yyyy')}
          style={styles.textInput1}
          editable={false}
        />
        <TextInput
          value={dateFormat(date, 'hh:MM')}
          style={styles.textInput1}
          editable={false}
        />
        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <View>
            <Button onPress={showDatepicker} title="Choose date" />
          </View>
          <View>
            <Button onPress={showTimepicker} title="Choose time" />
          </View>
        </View>
        {show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode={mode}
            is24Hour={true}
            display="default"
            onChange={onChange}
          />
        )}
        <TouchableOpacity style={styles.addBtn} onPress={() => callSave()}>
          <Text style={[styles.addText, {fontWeight: '400', fontSize: 14}]}>
            Save
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d9f1ff',
  },
  addBtn: {
    margin: 10,
    width: '95%',
    height: 40,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
    borderRadius: 5,
    // alignItems: 'flex-end',
    // alignSelf: 'flex-end',
  },
  addText: {
    alignSelf: 'center',
    color: 'white',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '60%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    justifyContent: 'center',
  },
  textStyle: {
    margin: 2,
    color: 'black',
  },
  textStyle1: {
    margin: 10,
    color: 'blue',
    fontWeight: '400',
  },
  textInput1: {
    margin: 12,
    borderBottomWidth: 1,
    padding: 10,
    width: '95%',
    height: 40,
    borderBottomColor: '#000',
  },
});

export default NewPlan;

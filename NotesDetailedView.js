import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
} from 'react-native';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import dateFormat from 'dateformat';

const NotesDetailedView = ({route}) => {
  const {item} = route.params;

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          margin: 10,
          backgroundColor: 'white',
          padding: 10,
          borderRadius: 5,
        }}>
        <Text style={styles.textStyle}>{item.projectname}</Text>
        <Text style={styles.textStyle}>{item.projectdesc}</Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d9f1ff',
  },
  addBtn: {
    margin: 10,
    width: '95%',
    height: 40,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
    borderRadius: 5,
    // alignItems: 'flex-end',
    // alignSelf: 'flex-end',
  },
  addText: {
    alignSelf: 'center',
    color: 'white',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '60%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    justifyContent: 'center',
  },
  textStyle: {
    margin: 2,
    color: 'black',
  },
  textStyle1: {
    margin: 10,
    color: 'blue',
    fontWeight: '400',
  },
});

export default NotesDetailedView;

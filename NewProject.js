import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const NewProject = ({route}) => {
  const {verifiedUserJob} = route.params;
  const {verifiedUserName} = route.params;

  const [click, setClick] = useState(false);
  const [valueOne, setValueOne] = useState('');
  const [valueTwo, setValueTwo] = useState('');
  const [dummy, setDummy] = useState('dummy');

  const [data, setData] = useState([]);
  const [response, setResponse] = useState([]);

  const navigation = useNavigation();

  function saveProjectData() {
    if (valueOne != '' && valueTwo != '') {
      setData([
        ...data,
        {id: valueOne, name: valueTwo, createdBy: verifiedUserName, tasks: []},
      ]);
      setClick(false);
      // setValueOne('');
      // setValueTwo('');
      callProject();
    } else {
      Alert.alert('Please enter all the fields');
    }
  }

  function callProject() {
    fetch('http://192.168.0.188:5000/project/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Connection: 'keep-alive',
      },
      body: JSON.stringify({
        projectname: valueTwo,
        projectdesc: valueOne,
        username: verifiedUserName,
        status: 'Pending',
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson == 'Project added!') {
          navigation.navigate('Home', {verifiedUserName, verifiedUserJob});
        } else {
          console.log('Add project failed');
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          backgroundColor: 'white',
          height: 580,
          margin: 10,
          borderRadius: 5,
        }}>
        <TextInput
          value={valueTwo}
          style={styles.textInput1}
          onChangeText={text => setValueTwo(text)}
          placeholder={'Title'}
        />
        <Text
          style={{width: '35%', marginLeft: 10, marginBottom: 5, marginTop: 5}}>
          Description
        </Text>
        <View
          style={{
            height: 400,
            borderWidth: 1,
            borderColor: 'black',
            width: '95%',
            margin: 10,
            borderRadius: 5,
          }}>
          <TextInput
            value={valueOne}
            style={styles.textInput}
            multiline={true}
            onChangeText={text => setValueOne(text)}
          />
        </View>
        <TouchableOpacity
          style={styles.textInputAddBtn}
          onPress={() => saveProjectData()}>
          <Text style={styles.addText}>Save</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d9f1ff',
  },
  addBtn: {
    margin: 10,
    width: '95%',
    height: 40,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
    borderRadius: 5,
    // alignItems: 'flex-end',
    // alignSelf: 'flex-end',
  },
  addText: {
    alignSelf: 'center',
    color: 'white',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    padding: 10,
    width: '95%',
    height: '100%',
  },
  textInput1: {
    margin: 12,
    borderBottomWidth: 1,
    padding: 10,
    width: '95%',
    height: 40,
    borderBottomColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
    borderRadius: 5,
  },
  touchableOpacityStyle: {
    height: 80,
    justifyContent: 'center',
  },
  textStyle: {
    margin: 2,
  },
});

export default NewProject;

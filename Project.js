import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
} from 'react-native';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import dateFormat from 'dateformat';

const Project = ({route}) => {
  const {verifiedUserJob} = route.params;
  const {verifiedUserName} = route.params;

  var screenView = useIsFocused();

  const [click, setClick] = useState(false);
  const [valueOne, setValueOne] = useState('');
  const [valueTwo, setValueTwo] = useState('');
  const [dummy, setDummy] = useState('dummy');
  const [data, setData] = useState([]);
  const [response, setResponse] = useState([]);

  const [focus, setFocus] = useState(screenView);

  const navigation = useNavigation();

  useEffect(() => {
    callProjectGet();
    setFocus(screenView);
  }, [screenView, focus]);

  function callProjectGet() {
    fetch('http://192.168.0.188:5000/project', {
      method: 'GET',
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson != null) {
          setResponse(responseJson);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  function callDelete(item) {
    var id = item._id;
    fetch('http://192.168.0.188:5000/project/' + id, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Connection: 'keep-alive',
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson == 'Project deleted!') {
          callProjectGet();
          console.log('deleted');
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  return (
    <SafeAreaView style={styles.container}>
      {verifiedUserJob == 'admin' ? (
        <View>
          <TouchableOpacity
            style={styles.addBtn}
            onPress={() =>
              navigation.navigate('NewNotes', {
                verifiedUserName,
                verifiedUserJob,
              })
            }>
            <Text style={[styles.addText, {fontWeight: '400', fontSize: 14}]}>
              Add notes
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.addBtn}
            onPress={() =>
              navigation.navigate('Tasks', {
                verifiedUserName,
              })
            }>
            <Text style={[styles.addText, {fontWeight: '400', fontSize: 14}]}>
              Add to-do list
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.addBtn}
            onPress={() =>
              navigation.navigate('Planner', {
                verifiedUserName,
              })
            }>
            <Text style={[styles.addText, {fontWeight: '400', fontSize: 14}]}>
              Planner
            </Text>
          </TouchableOpacity>
        </View>
      ) : null}
      <View>
        {response != null && response.length > 0 ? (
          <FlatList
            data={response}
            keyExtractor={item => item.id}
            renderItem={({item}) => (
              <View
                style={{
                  padding: 10,
                  backgroundColor: 'white',
                  margin: 10,
                  borderRadius: 5,
                }}>
                <TouchableOpacity
                  style={styles.touchableOpacityStyle}
                  onPress={() => {
                    navigation.navigate('NotesDetail', {item});
                  }}>
                  <View style={{marginTop: 10, marginBottom: 10}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={styles.textStyle}>{item.projectname}</Text>
                      {/* <TouchableOpacity
                        onPress={() =>
                          navigation.navigate('NewNotes', {
                            verifiedUserName,
                            item,
                          })
                        }>
                        <Icon name="edit" color={'black'} size={20} />
                      </TouchableOpacity> */}
                      <TouchableOpacity onPress={() => callDelete(item)}>
                        <Icon name="delete" color={'black'} size={20} />
                      </TouchableOpacity>

                      {/* <Text
                        style={
                          item.status == 'Completed'
                            ? styles.textStyle1
                            : styles.textStyle
                        }>
                        {item.status}
                      </Text> */}
                    </View>
                    <Text numberOfLines={2} style={styles.textStyle}>
                      {item.projectdesc}
                    </Text>
                    <Text style={(styles.textStyle, {alignSelf: 'flex-end'})}>
                      {dateFormat(item.createdAt, 'mmm dd, yyyy')}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          />
        ) : null}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d9f1ff',
  },
  addBtn: {
    margin: 10,
    width: '95%',
    height: 40,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
    borderRadius: 5,
    // alignItems: 'flex-end',
    // alignSelf: 'flex-end',
  },
  addText: {
    alignSelf: 'center',
    color: 'white',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '60%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: '#0da2ff',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    justifyContent: 'center',
  },
  textStyle: {
    margin: 2,
    color: 'black',
  },
  textStyle1: {
    margin: 10,
    color: 'blue',
    fontWeight: '400',
  },
});

export default Project;

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Project from './Project';
import ProjectDetails from './ProjectDetails';
import LoginScreen from './LoginScreen';
import RegisterScreen from './Register';
import TaskDetails from './TaskDetails';
import NewProject from './NewProject';
import NewTask from './AddNewTask';
import NotesDetailedView from './NotesDetailedView';
import Planner from './Planner';
import NewPlan from './NewPlan';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen name="Register" component={RegisterScreen} />
        <Stack.Screen name="Home" component={Project} />
        <Stack.Screen name="Tasks" component={ProjectDetails} />
        <Stack.Screen name="Task" component={TaskDetails} />
        <Stack.Screen name="NewNotes" component={NewProject} />
        <Stack.Screen name="NewTask" component={NewTask} />
        <Stack.Screen name="NotesDetail" component={NotesDetailedView} />
        <Stack.Screen name="Planner" component={Planner} />
        <Stack.Screen name="NewPlan" component={NewPlan} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
